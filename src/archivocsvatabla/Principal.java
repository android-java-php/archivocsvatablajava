/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package archivocsvatabla;

import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Josue Daniel Roldan Ochoa.
 */
public class Principal extends javax.swing.JFrame {
DefaultTableModel modelo;
    /**
     * Creates new form Principal
     */
    public Principal() {
    initComponents();
    tiTulos();
    this.setLocationRelativeTo(null);
    }
private void tiTulos(){
Object[]titulos={"Codigo","Nombre","Email"};
modelo=new DefaultTableModel(null,titulos);
tbl1.setModel(modelo);
}
 
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        tbl1 = new javax.swing.JTable();
        jScrollPane2 = new javax.swing.JScrollPane();
        txtexto = new javax.swing.JTextArea();
        jLabel1 = new javax.swing.JLabel();
        txcodigo = new javax.swing.JTextField();
        txnombre = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        txemail = new javax.swing.JTextField();
        jButton3 = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenuItem1 = new javax.swing.JMenuItem();
        jMenuItem2 = new javax.swing.JMenuItem();
        jMenuItem3 = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("ARCHIVOSCSV");
        setResizable(false);

        tbl1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        jScrollPane1.setViewportView(tbl1);

        txtexto.setColumns(20);
        txtexto.setRows(5);
        txtexto.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtextoKeyTyped(evt);
            }
        });
        jScrollPane2.setViewportView(txtexto);

        jLabel1.setText("Codigo:");

        txcodigo.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txcodigo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txcodigoActionPerformed(evt);
            }
        });
        txcodigo.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txcodigoKeyTyped(evt);
            }
        });

        txnombre.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txnombre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txnombreActionPerformed(evt);
            }
        });

        jLabel2.setText("Nombre:");

        jLabel3.setText("Email:");

        txemail.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txemail.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txemailActionPerformed(evt);
            }
        });

        jButton3.setText("Agregar");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        jButton1.setText("Cancelar");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jButton2.setText("Borrar Datos");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jMenu1.setText("Archivo");

        jMenuItem1.setText("Importar CSV");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem1);

        jMenuItem2.setText("Exportar CSV");
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem2);

        jMenuItem3.setText("Salir");
        jMenuItem3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem3ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem3);

        jMenuBar1.add(jMenu1);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txcodigo, javax.swing.GroupLayout.PREFERRED_SIZE, 129, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txnombre))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txemail))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jButton2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 131, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton3, javax.swing.GroupLayout.PREFERRED_SIZE, 142, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
            .addComponent(jScrollPane2)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(txcodigo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2)
                    .addComponent(txnombre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(26, 26, 26)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(txemail, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton3)
                    .addComponent(jButton1)
                    .addComponent(jButton2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 94, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 124, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem2ActionPerformed
JFileChooser flc=new JFileChooser();
flc.showSaveDialog(this);
String fx=flc.getSelectedFile().toString();
File file=new File(fx+".csv");
if(file!=null){
try {
FileWriter save=new FileWriter(file,true);
BufferedWriter br=new BufferedWriter(save);
br.write(txtexto.getText());
br.flush();
br.close();
JOptionPane.showMessageDialog(null,"Archivo Guardado Correctamente!!!","INFORMACION",JOptionPane.INFORMATION_MESSAGE);
} catch (IOException ex) {
}
}  
    }//GEN-LAST:event_jMenuItem2ActionPerformed

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
String aux="";
String texto ="";
String []spl = null;
int cont=0;
JFileChooser fc=new JFileChooser();
fc.showOpenDialog(this);
File fl=fc.getSelectedFile();
if(fl!=null){
try {
FileReader fr=new FileReader(fl);
BufferedReader lee=new BufferedReader(fr);
while((aux=lee.readLine())!=null){
//texto=aux;
if(texto.length()!=0){
if(txtexto.getText().length()!=0){
if(cont==0){
texto=txtexto.getText()+"\n"+aux;
System.out.println(texto);
cont=1;
}else{
texto=texto+"\n"+aux;
}
}else{
texto=texto+"\n"+aux;
}
}else{
if(txtexto.getText().length()!=0){   
texto=txtexto.getText()+"\n"+aux;
cont=1;
}else{
texto=aux;

}
}
spl=aux.split(",");
modelo.addRow(spl);

}
txtexto.setText(texto);
lee.close();

} catch (IOException ex) {
}

} 
        // TODO add your handling code here:
    }//GEN-LAST:event_jMenuItem1ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
String codigo,nombre,email,texto;
codigo=txcodigo.getText();
nombre=txnombre.getText();
email=txemail.getText();
if(codigo.length()!=0&&nombre.length()!=0&&email.length()!=0){
Object[]ob={codigo,nombre,email};
modelo.addRow(ob);
if(txtexto.getText().length()!=0){
texto=txtexto.getText();
texto=texto+"\n"+codigo+","+nombre+","+email;
txtexto.setText(texto);
}else{
texto=codigo+","+nombre+","+email;
txtexto.setText(texto);
}
txcodigo.setText("");
txnombre.setText("");
txemail.setText("");
}else
if(codigo.length()!=0&&nombre.length()!=0&&email.length()==0){
JOptionPane.showMessageDialog(null,"FALTAN DATOS!!!","INFORMACION",JOptionPane.INFORMATION_MESSAGE);
}else
if(codigo.length()!=0&&nombre.length()==0&&email.length()==0){
JOptionPane.showMessageDialog(null,"FALTAN DATOS!!!","INFORMACION",JOptionPane.INFORMATION_MESSAGE);
}else
if(codigo.length()==0&&nombre.length()==0&&email.length()==0){
JOptionPane.showMessageDialog(null,"FALTAN DATOS!!!","INFORMACION",JOptionPane.INFORMATION_MESSAGE);
}else
if(codigo.length()!=0&&nombre.length()==0&&email.length()!=0){
JOptionPane.showMessageDialog(null,"FALTAN DATOS!!!","INFORMACION",JOptionPane.INFORMATION_MESSAGE);
}else
if(codigo.length()==0&&nombre.length()!=0&&email.length()!=0){
JOptionPane.showMessageDialog(null,"FALTAN DATOS!!!","INFORMACION",JOptionPane.INFORMATION_MESSAGE);
}else
if(codigo.length()==0&&nombre.length()==0&&email.length()!=0){
JOptionPane.showMessageDialog(null,"FALTAN DATOS!!!","INFORMACION",JOptionPane.INFORMATION_MESSAGE);
}

txcodigo.grabFocus();

    }//GEN-LAST:event_jButton3ActionPerformed

    private void jMenuItem3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem3ActionPerformed
System.exit(0);    
    }//GEN-LAST:event_jMenuItem3ActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
txcodigo.setText("");
txnombre.setText("");
txemail.setText("");
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
txtexto.setText("");
int cont=tbl1.getRowCount();
if(cont!=0){
for(int n=0; n<cont;n++){
modelo.removeRow(0);

}

}

    }//GEN-LAST:event_jButton2ActionPerformed

    private void txemailActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txemailActionPerformed
String codigo,nombre,email,texto;
codigo=txcodigo.getText();
nombre=txnombre.getText();
email=txemail.getText();
if(codigo.length()!=0&&nombre.length()!=0&&email.length()!=0){
Object[]ob={codigo,nombre,email};
modelo.addRow(ob);
if(txtexto.getText().length()!=0){
texto=txtexto.getText();
texto=texto+"\n"+codigo+","+nombre+","+email;
txtexto.setText(texto);
}else{
texto=codigo+","+nombre+","+email;
txtexto.setText(texto);
}
txcodigo.setText("");
txnombre.setText("");
txemail.setText("");
}else
if(codigo.length()!=0&&nombre.length()!=0&&email.length()==0){
JOptionPane.showMessageDialog(null,"FALTAN DATOS!!!","INFORMACION",JOptionPane.INFORMATION_MESSAGE);
}else
if(codigo.length()!=0&&nombre.length()==0&&email.length()==0){
JOptionPane.showMessageDialog(null,"FALTAN DATOS!!!","INFORMACION",JOptionPane.INFORMATION_MESSAGE);
}else
if(codigo.length()==0&&nombre.length()==0&&email.length()==0){
JOptionPane.showMessageDialog(null,"FALTAN DATOS!!!","INFORMACION",JOptionPane.INFORMATION_MESSAGE);
}else
if(codigo.length()!=0&&nombre.length()==0&&email.length()!=0){
JOptionPane.showMessageDialog(null,"FALTAN DATOS!!!","INFORMACION",JOptionPane.INFORMATION_MESSAGE);
}else
if(codigo.length()==0&&nombre.length()!=0&&email.length()!=0){
JOptionPane.showMessageDialog(null,"FALTAN DATOS!!!","INFORMACION",JOptionPane.INFORMATION_MESSAGE);
}else
if(codigo.length()==0&&nombre.length()==0&&email.length()!=0){
JOptionPane.showMessageDialog(null,"FALTAN DATOS!!!","INFORMACION",JOptionPane.INFORMATION_MESSAGE);
}
txcodigo.grabFocus();
    }//GEN-LAST:event_txemailActionPerformed

    private void txcodigoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txcodigoActionPerformed
txnombre.grabFocus();        // TODO add your handling code here:
    }//GEN-LAST:event_txcodigoActionPerformed

    private void txnombreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txnombreActionPerformed
txemail.grabFocus();        // TODO add your handling code here:
    }//GEN-LAST:event_txnombreActionPerformed

    private void txcodigoKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txcodigoKeyTyped
int k = (int) evt.getKeyChar();
if (k > 58 && k < 255 ) {//Si el caracter ingresado es una letra
evt.setKeyChar((char) KeyEvent.VK_CLEAR);//Limpiar el caracter ingresado
JOptionPane.showMessageDialog(null, "Solo puede Ingresar numeros!!!", "Validando Datos",
JOptionPane.ERROR_MESSAGE);
}   
int d = (int) evt.getKeyChar();
if (d > 32 && d < 47) {//Si el caracter ingresado es una letra
evt.setKeyChar((char) KeyEvent.VK_CLEAR);//Limpiar el caracter ingresado
JOptionPane.showMessageDialog(null, "Solo puede Ingresar numeros!!!", "Validando Datos",
JOptionPane.ERROR_MESSAGE);

}        // TODO add your handling code here:
    }//GEN-LAST:event_txcodigoKeyTyped

    private void txtextoKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtextoKeyTyped
int cont =modelo.getRowCount();
for(int x=0; x<cont; x++){
modelo.removeRow(0);
}
String [] array,array2;
array=txtexto.getText().split("\n");
int n=array.length;
for(int nx=0;nx<n;nx++){
array2=array[nx].split(",");
int n2=array2.length;
if(n2==3){
modelo.addRow(array2);
}if(n2>3){
if(array2[3].length()!=0){
int cn,cn2 = 0;
for(int cx=0;cx<n; cx++){
cn =array[cx].length();
cn2+=cn;
}
cn2=cn2-2;
System.out.println(cn2);
txtexto.setEnabled(false);
txtexto.setText(txtexto.getText().substring(0,cn2)+"\r");


JOptionPane.showMessageDialog(null,"Cantidad de filas incorrectas: "+"Total columnas:"+modelo.getColumnCount());
//System.out.println("Cantidad de filas incorrectas: "+"Total columnas:"+modelo.getColumnCount());
txtexto.setEnabled(true);
int j= txtexto.getText().length();
j=j-2;
txtexto.setText(txtexto.getText().substring(0,j));
}

}else{
//System.out.println("Columna invalida");
}
}        // TODO add your handling code here:
    }//GEN-LAST:event_txtextoKeyTyped

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Principal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Principal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Principal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Principal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Principal().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JMenuItem jMenuItem3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable tbl1;
    private javax.swing.JTextField txcodigo;
    private javax.swing.JTextField txemail;
    private javax.swing.JTextField txnombre;
    private javax.swing.JTextArea txtexto;
    // End of variables declaration//GEN-END:variables
}
